#include <stm32f0xx_gpio.h>
#include <stm32f0xx_rcc.h>
#include <stm32f0xx_usart.h>
#include <stdio.h>
#include "MotorDriver.h"
#include "CommandProcessor.h"
#include "Usart.h"


__IO uint32_t _delay;
__IO int32_t _pos;
void Delay(uint32_t time)
{
	_delay = time;
	while (_delay)
		;
	
}

void USARTInit(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	/* USARTx configured as follow:
	- BaudRate = 115200 baud
	- Word Length = 8 Bits
	- One Stop Bit
	- No parity
	- Hardware flow control disabled (RTS and CTS signals)
	- Receive and transmit enabled
	*/
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	/* Enable GPIO clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

	/* Enable USART clock */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	/* Connect PXx to USARTx_Tx */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_1);

	/* Connect PXx to USARTx_Rx */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_1);

	/* Configure USART Tx, Rx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* USART configuration */
	USART_Init(USART1, &USART_InitStructure);

	/* Enable USART */
	USART_Cmd(USART1, ENABLE);

}

//#define A1Pin GPIO_Pin_0
//#define A2Pin GPIO_Pin_3
//#define B1Pin GPIO_Pin_1
//#define B2Pin GPIO_Pin_2




void Run2()
{
	steppers[0].Position = 1;
	SetStep(&steppers);
	
	ResetPwm();
	
	UpdatePwm(1);
}

void Run1(void)
{

	TIM_Config();
	printf("run\r\n");

	StepperDef* stepper = &steppers[0];
	steppers[0].Speed = 0x04f;
	steppers[1].Speed = 0x04f;
	//steppers[0].MoveTo = 50;
	//stepper->Speed = 0x8f;
	//stepper->MoveTo = 50;
	//c_moveTo(1, 1);
	
	/**/
	//testPrint();
	for (;;)
	{
		//__disable_irq();

		char* cmdLine = getCmd();
		

		if (cmdLine != 0)
		{ 
			printf("Exec:%s\r\n", cmdLine);
			executeCommand(cmdLine);
			releaseLastCmd();
		}
		//__enable_irq();
	}
	printf("end run\r\n");
}

int  main()
{
	SysTick_Config(SystemCoreClock / 10000 - 1);
	//USARTInit();
	initUSART();
	printf("num:%d  text:%s \r\n", 123, "qqq");
	//Delay(100);
	printf("ok\r\n");
	
	
	
	
	//return;
	Run1();
	Run2();
	char i = 0;
	printf("end\r\n");
	
}


void SysTick_Handler(void)
{
	if (_delay)
		_delay--;

}

void HardFault_Handler(void)
{
	GPIOC->BSRR = GPIO_Pin_8;
	printf("***Hand fault_error\r\n");
}
