#include <stm32f0xx_usart.h>
#include <stm32f0xx_gpio.h>
#include <stm32f0xx_rcc.h>
#include "stm32f0xx_misc.h"
#include "Usart.h"
#include <stdio.h>





volatile char txBuffer[TX_BUF_SIZE];
volatile int txWriteIdx = 0;
volatile int txSendIdx = 0;

volatile char * activeLine;

volatile char rxLines[2][RX_BUF_SIZE];
volatile int rxLineIdx = 0;
volatile int rxCharIdx = 0;







void initUSART()
{
	
	NVIC_InitTypeDef NVIC_InitStructure;

	  /* Enable the USARTx Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_1);

		/* Connect PXx to USARTx_Rx */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_1);

	GPIO_InitTypeDef gpio;

	/* Configure USART Tx, Rx as alternate function push-pull */
	gpio.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;
	gpio.GPIO_Mode = GPIO_Mode_AF;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_OType = GPIO_OType_PP;
	gpio.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, &gpio);

	
	USART_InitTypeDef usart;
	usart.USART_BaudRate = 115200;
	usart.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	usart.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	usart.USART_Parity = USART_Parity_No;
	usart.USART_StopBits = USART_StopBits_1;
	usart.USART_WordLength = USART_WordLength_8b;
	USART_Init(USART1, & usart);
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	USART_Cmd(USART1, ENABLE);
}


	int _write(int fd, char *pBuffer, int size)
	{
		__disable_irq();

		for (int i = 0; i < size; i++)
		{
			txBuffer[txWriteIdx++] = pBuffer[i];
			if (txWriteIdx == TX_BUF_SIZE) txWriteIdx = 0;
		}
		USART_ITConfig(USART1, USART_IT_TXE, ENABLE);	

		__enable_irq();

		return size;
	}
/**/

void USART1_IRQHandler(void)
{
	if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
	{
	  /* Read one byte from the receive data register */
		char c = (USART_ReceiveData(USART1) & 0xFF);
		//printf(c);
		//USART_SendData(USART1, c);
		if (c == 10 || c == 13) {
			if (rxCharIdx == 0) return;
			rxLines[rxLineIdx][rxCharIdx] = 0;
			rxCharIdx = 0;
			if (activeLine == 0) {
				activeLine = rxLines[rxLineIdx];
				rxLineIdx ^= 1;
			}
		}
		else {
			rxLines[rxLineIdx][rxCharIdx++] = c;
			if (rxCharIdx == RX_BUF_SIZE - 2) rxCharIdx = 0;
		}
		
	}

	if (USART_GetITStatus(USART1, USART_IT_TXE) != RESET)
	{   
	  /* Write one byte to the transmit data register */
		USART_SendData(USART1, txBuffer[txSendIdx++]);
		if (txSendIdx == TX_BUF_SIZE) txSendIdx = 0;
		if (txSendIdx == txWriteIdx)
		{
		  /* Disable the USART1 Transmit interrupt */
			USART_ITConfig(USART1, USART_IT_TXE, DISABLE);
		}
	}
}


char * getCmd(void) {
	return (char*)activeLine;
}
void releaseLastCmd(void) {
	activeLine = 0;
}