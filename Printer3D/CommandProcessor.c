#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "MotorDriver.h"
#include "gcode.h"
#include <stm32f0xx_gpio.h>

#define StepBY_MM (double) 27.1186
const uint16_t  c_speed = 40;

void c_move(int32_t x, int32_t y, int32_t z);
void c_move_mm(double x, double y,double z,double e);


#define EXTRUDER_OFF GPIOC->BSRR = GPIO_BSRR_BS_9
#define EXTRUDER_ON GPIOC->BSRR = GPIO_BSRR_BR_9

//void c_c1(int size)
//{
		////c_move(10, 10);
		////c_move(420, 130);
		////c_move(-420, -130);
		//
	///*
	//c_move(size, 0);
	//c_move(0, size);
	//c_move(-size, 0);
	//c_move(0, -size);
		//*/
	//c_move(size, size/2);
	//c_move(-size, size / 2);
		//
	//c_move(size / 2, -size);
	//c_move(size / 2, size);
		//
	//c_move(-size, -size / 2);
	//c_move(size, -size / 2);
		//
	//c_move(-size / 2, size);
	//c_move(-size / 2, -size);
		//
	////c_moveTo(0, 0);
		//
//
//}

void executeCommand(const char* cmdLine) {
	int X=0;
	int Y=0;
	char str[10];
	
	//if (sscanf(cmdLine, "c%d", &X) == 1)
	//{
		//c_c1(700);
		//return;
	//}
	
	gc_execute_line(cmdLine);
	//int argCount = sscanf(cmdLine, "X%d Y%d", &X, &Y);
	printf("ok \r\n");
	
	return;
	//StepperDef* stepper = &steppers[0];

	//c_move_mm(X, 0);
	return;
	//if (argCount == 2)
		//steppers[0].MoveTo += Y;
	//if (argCount > 0)
	//{
		//steppers[1].MoveTo += X;
	//}
}

//void c_moveTo(int32_t x, int32_t y)
//{
	//steppers[0].MoveTo = x;
	//steppers[1].MoveTo = y;
	//while (steppers[0].MoveTo != steppers[0].Position || steppers[1].MoveTo != steppers[1].Position)
		//asm("nop");
	//
//}
int c_checkSpeed(int32_t x, int32_t y)
{
	if (x < 0)
		x *= -1;
	if (y < 0)
		y *= -1;
	int c;
	//steppers[0].Speed = c_speed;
	if (x > y)
	{
		steppers[1].Speed = c_speed*x / y;
		c = steppers[1].Speed*y / x;
	}
	else
	{
		steppers[0].Speed = c_speed*y / x;
		c = steppers[0].Speed*x / y;

	 }
	return c;
}

void c_move(int32_t x, int32_t y, int32_t z)
{
	steppers[0].Speed = c_speed;
	steppers[1].Speed = c_speed;
	steppers[2].Speed = c_speed;

	if (x && y)
		c_checkSpeed(x, y);
	
	printf("c_move dX:%d dY:%d dZ:%d\r\n", x, y,z);

	steppers[0].MoveTo += x;
	steppers[1].MoveTo += y;
	steppers[2].MoveTo += z;
	
	
	
	while (steppers[0].MoveTo != steppers[0].Position 
		|| steppers[1].MoveTo != steppers[1].Position
		|| steppers[2].MoveTo != steppers[2].Position
		)
		asm("nop");
	
}

void c_move_mm(double x, double y,double z,double e)
{
	int32_t stepX = x*StepBY_MM;
	int32_t stepY = y*StepBY_MM;
	int32_t stepZ = z*40;
	
	int32_t zPos = steppers[2].Position;
	
	int coef = 10;
	if (zPos > 200)
		coef = 20;
	stepX -= zPos / coef;
	
	stepX -= steppers[0].Position;
	stepY -= steppers[1].Position;
	stepZ -= steppers[2].Position;

	if (e > 0)
	{
		EXTRUDER_ON;
		if (stepX==0&&
			stepY==0&&
			stepZ==0)
		{
			Delay(200);
		}
	}

	
	c_move(stepX, stepY,stepZ);

	EXTRUDER_OFF;
	GPIOC->BSRR = GPIO_BSRR_BS_9;
}

void cnc_unretract()
{
	printf("unretract\r\n");
	EXTRUDER_ON;
	Delay(5000);
	EXTRUDER_OFF;

}
void cnc_end()
{
	steppers[0].MoveTo = steppers[0].Position;
	steppers[1].MoveTo = steppers[1].Position;
	steppers[2].MoveTo = steppers[2].Position;
	EXTRUDER_OFF;
}
uint8_t cnc_line(double x,
	double y,
	double z,
	double extruder_length, 
	double length,
	double feed_rate)
{
	
	printf("Move X:%d Y:%d Z:%d E:%d  \r\n", (int)x,(int)y,(int)z, (int)extruder_length);


	c_move_mm(x, y,z,extruder_length);

}


