#include "stdint.h"
#include <stm32f0xx_gpio.h>


#define stepCount 16

typedef struct 
{
	uint16_t Pin;
	GPIO_TypeDef* Port;
	uint16_t Counter;
	uint16_t Current;

} PWMDef;

typedef struct
{
	uint8_t A1;
	uint8_t A2;
	uint8_t B1;
	uint8_t B2;
} StepDef;


typedef struct
{
	PWMDef* A1;
	PWMDef* A2;
	PWMDef* B1;
	PWMDef* B2;
	int32_t Position;
	int32_t Zerro;
	int32_t MoveTo;
	int32_t Timeout;
	int16_t Speed;
	
} StepperDef;

extern StepperDef steppers[];

void TIM_Config(void);
void InitGPIOMotor();

void ResetPwm();
void UpdatePwm(uint8_t counter);
void Move();
void SetStep(StepperDef* stepper);