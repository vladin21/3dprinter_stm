#define TX_BUF_SIZE 200

#define RX_BUF_SIZE 200

void initUSART();
char* getCmd(void);
void releaseLastCmd(void);


extern volatile char txBuffer[];
extern volatile int txWriteIdx;
extern volatile int txSendIdx;

extern volatile char * activeLine;

extern volatile char rxLines[2][RX_BUF_SIZE];
extern volatile int rxLineIdx;
extern volatile int rxCharIdx;
