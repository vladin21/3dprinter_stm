#include <stm32f0xx.h>
#include <stm32f0xx_gpio.h>
#include <stm32f0xx_rcc.h>
#include "MotorDriver.h"


#define PWMCount (char)12
#define SteppersCount 3

#define PWMPort GPIOA

const uint8_t stepValue[5] = { 0, 97, 180, 236, 255 };
__IO uint32_t speedCounter;

PWMDef* active_pwms[PWMCount];
char active_count;

const StepDef StepMask[stepCount] = {
	{ 4, 0, 0, 0 },
	{ 3, 1, 0, 0 },
	{ 2, 2, 0, 0 },
	{ 1, 3, 0, 0 },
	{ 0, 4, 0, 0 },
	{ 0, 3, 1, 0 },
	{ 0, 2, 2, 0 },
	{ 0, 1, 3, 0 },
	{ 0, 0, 4, 0 },
	{ 0, 0, 3, 1 },
	{ 0, 0, 2, 2 },
	{ 0, 0, 1, 3 },
	{ 0, 0, 0, 4 },
	{ 1, 0, 0, 3 },
	{ 2, 0, 0, 2 },
	{ 3, 0, 0, 1 },
};

PWMDef pwms[PWMCount] = {
	{ GPIO_Pin_4, GPIOA },
	{ GPIO_Pin_6, GPIOA },
	{ GPIO_Pin_5, GPIOA },
	{ GPIO_Pin_7, GPIOA },

	{ GPIO_Pin_0, GPIOA },
	{ GPIO_Pin_2, GPIOA },
	{ GPIO_Pin_1, GPIOA },
	{ GPIO_Pin_3, GPIOA },
		
	{ GPIO_Pin_0, GPIOC },
	{ GPIO_Pin_3, GPIOC },
	{ GPIO_Pin_1, GPIOC },
	{ GPIO_Pin_2, GPIOC },
		
};



StepperDef steppers[SteppersCount] = { 
	{ &pwms[0], &pwms[1], &pwms[2], &pwms[3] },
	{ &pwms[4], &pwms[5], &pwms[6], &pwms[7] }, 
	{ &pwms[8], &pwms[9], &pwms[10], &pwms[11] }, 
};

void ResetPwm()
{
	int8_t i;
	PWMDef* pwm = &pwms;
	active_count = 0;
	for (i = 0; i < PWMCount; i++)
	{
		
		pwm->Current = pwm->Counter;
		if (pwm->Current)
		{
			active_pwms[active_count++] = pwm;
			pwm->Port->BSRR = pwm->Pin;
		}
		pwm++;
	}
	

}

void UpdatePwm(uint8_t counter)
{
	//uint16_t value = PWMPort->ODR;
	PWMDef* pwm;
	//PWMDef* end = ((PWMDef*)&pwms) + PWMCount;
	
	int i = 0;


	
	while (i<active_count)
	{
		pwm = active_pwms[i++];
		if (!(pwm->Current ^ counter))
		{
			pwm->Port->BSRR = pwm->Pin << 16;
		}
	}
	//PWMPort->ODR = value;

}

void SetStep(StepperDef* stepper)
{
	uint8_t i = ((uint32_t)stepper->Position) % stepCount;

	StepDef* step = (StepDef*)&StepMask[i];

	stepper->A1->Counter = stepValue[step->A1];
	stepper->A2->Counter = stepValue[step->A2];
	stepper->B1->Counter = stepValue[step->B1];
	stepper->B2->Counter = stepValue[step->B2];
	
	//printf("Set step %d\r\n", i);
}

void Reset(StepperDef* stepper)
{
	stepper->A1->Counter = 0;
	stepper->A2->Counter = 0;
	stepper->B1->Counter = 0;
	stepper->B2->Counter = 0;
}
void InitGPIOMotor()
{
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);

	GPIO_InitTypeDef        gpio;
	gpio.GPIO_Pin = GPIO_Pin_8;
	/* Configure PC8 and PC9 in output pushpull mode */

	gpio.GPIO_Mode = GPIO_Mode_OUT;
	gpio.GPIO_OType = GPIO_OType_PP;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
	
	GPIO_Init(GPIOA, &gpio);

	for (int i = 0; i < PWMCount; i++)
	{
		gpio.GPIO_Pin = pwms[i].Pin;
		GPIO_Init(pwms[i].Port, &gpio);
	}
	
	gpio.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_8;
	GPIO_Init(GPIOC, &gpio);
	
	GPIOC->BSRR = GPIO_Pin_9;
	
	
	
}

void Move()
{
	speedCounter++;
	for (int i = 0; i < SteppersCount; i++)
	{
			
		StepperDef* stepper = &steppers[i];
		if (stepper->Position == stepper->MoveTo)
		{
			if (stepper->Timeout == 0xffff)
				continue;
			stepper->Timeout++;
			if (stepper->Timeout > 1000)
			{
				stepper->Timeout = 0xffff;
				Reset(stepper);
			}
		}
		else
		{
			stepper->Timeout = 0;
			if (!(speedCounter % stepper->Speed))
			{

				if (stepper->Position < stepper->MoveTo)
					stepper->Position++;
				else
					stepper->Position--;
				SetStep(stepper);
			}
		}
	}
}


